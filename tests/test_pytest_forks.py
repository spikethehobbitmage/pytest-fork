import os

from pytest import fail, mark, raises

def test_forks_no_fork():
	from pytest_forks import forks
	
	with forks():
		pass

def test_forks_clean_child():
	from pytest_forks import forks

	with forks():
		pid = os.fork()
		if pid == 0:
			os._exit(0)
		os.waitpid(pid, 0)

def test_nested_fork():
	from pytest_forks import forks

	with forks():
		pid = os.fork()
		if pid == 0:
			os.fork()
			os._exit(0)
		os.waitpid(pid, 0)

@mark.xfail
def test_forks_exit():
	from pytest_forks import forks

	with forks():
		pid = os.fork()
		if pid == 0:
			exit(0)
		os.waitpid(pid, 0)

@mark.xfail
def test_forks_no_exit():
	from pytest_forks import forks

	with forks():
		os.fork()

@mark.xfail
def test_child_raises():
	from pytest_forks import forks

	with forks():
		pid = os.fork()
		if pid == 0:
			raise ValueError('oops')
		os.waitpid(pid, 0)
	
@mark.xfail
def test_forks_bad_grandchild():
	from pytest_forks import forks

	with forks():
		pid = os.fork()
		if pid == 0:
			if os.fork() == 0:
				return
			os._exit(0)
		os.waitpid(pid, 0)
